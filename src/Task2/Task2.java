package Task2;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] ai = new int[n];
        for (int i = 0; i < n; i++) {
            ai[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        int[] aj = new int[m];
        for (int j = 0; j < m; j++) {
            aj[j] = scanner.nextInt();
        }
        boolean isEqualLength = (ai.length == aj.length);
        boolean isEqualsElements = true;
        if (isEqualLength) {
            for (int i = 0; i < ai.length; i++) {
                if (ai[i] != aj[i]) {
                    isEqualsElements = false;
                    break;
                }
            }
        }
        System.out.println(isEqualLength && isEqualsElements);
    }
}
