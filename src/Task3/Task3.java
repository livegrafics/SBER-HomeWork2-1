package Task3;

import java.util.Arrays;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        int x = scanner.nextInt();
        int pos = 0;
        for (int i = 0; i < a.length; i++) {
            if (x >= a[i] && x < a[i + 1]) {
                pos = i + 1;
                System.out.println(pos);
                break;
            }
        }
    }
}
