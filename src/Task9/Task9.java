package Task9;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[] s = new String[n];
        for (int i = 0; i < n; i++) {
            s[i] = scanner.next();
        }
        String result;
        boolean isFind = false;
        for (int i = 0; i < s.length - 1; i++) {
            result = s[i];
            for (int j = i + 1; j < s.length; j++) {
                if (result.equals(s[j])) {
                    System.out.println(result);
                    isFind = true;
                    break;
                }
            }
            if (isFind) {
                break;
            }
        }
    }
}
