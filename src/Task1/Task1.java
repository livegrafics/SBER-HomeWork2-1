package Task1;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double[] a = new double[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextDouble();
        }
        System.out.println(average(a));
    }

    public static double average(double[] array) {
        double average = 0;
        for (double a : array) {
            average = average + a;
        }
        return average / array.length;
    }
}
