package Task8;

import java.util.Arrays;
import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        int[] b = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            b[i] = (int) Math.abs(a[i] - m);
        }
        int minB = b[0];
        int posMinB = 0;
        for (int i = 1; i < b.length; i++) {
            if (minB > b[i]) {
                minB = b[i];
                posMinB = i;
            }
            if (minB == b[i]) {
                if (a[i] > a[posMinB]) {
                    posMinB = i;
                }
            }
        }
        System.out.println(a[posMinB]);
    }
}
