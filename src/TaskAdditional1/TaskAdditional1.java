package TaskAdditional1;

import java.util.Random;
import java.util.Scanner;

public class TaskAdditional1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int length = 0;
        String password = "";
        while (length < 8) {
            length = scanner.nextInt();
            if (length >= 8) {
                do {
                    password = generatePassword(length);
                } while (!checkPassword(password));
            } else {
                System.out.println("Пароль с " + length + " количеством символов небезопасен");
            }
        }
        System.out.println(password);
    }

    public static String generatePassword(int length) {
        Random random = new Random(System.currentTimeMillis());
        int count = 0;
        String password = "";
        while (count < length) {
            char symbol = (char) random.nextInt(32767);
            if ((symbol >= 'a' && symbol <= 'z') || (symbol >= 'A' && symbol <= 'Z') || (symbol >= '0' && symbol <= '9')
                    || symbol == '_' || symbol == '*' || symbol == '-') {
                password = password + symbol;
                ++count;
            }
        }
        return password;
    }

    public static boolean checkPassword(String password) {
        int titles = 0, lowercases = 0, numbers = 0, signs = 0;
        for (int i = 0; i < password.length(); i++) {
            if ((password.charAt(i) >= 'A' && password.charAt(i) <= 'Z')) {
                ++titles;
            }
            if ((password.charAt(i) >= 'a' && password.charAt(i) <= 'z')) {
                ++lowercases;
            }
            if (password.charAt(i) >= '0' && password.charAt(i) <= '9') {
                ++numbers;
            }
            if (password.charAt(i) == '_' || password.charAt(i) == '*' || password.charAt(i) == '-') {
                ++signs;
            }
        }
        if (titles > 0 && lowercases > 0 && numbers > 0 && signs > 0) {
            return true;
        } else {
            return false;
        }
    }
}
