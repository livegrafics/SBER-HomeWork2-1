package Task6;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        String[] morze = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.",
                "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};
        Scanner scanner = new Scanner(System.in);
        String s = scanner.next();
        for (int i = 0; i < s.length(); i++) {
            char a = s.charAt(i);
            System.out.print(morze[a - 'А'] + " ");
        }
    }
}
