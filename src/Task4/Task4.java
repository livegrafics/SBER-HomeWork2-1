package Task4;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        int b = a[0];
        int count = 0;
        for (int i = 0; i < a.length; i++) {
            if (b == a[i]) {
                ++count;
            } else {
                System.out.println(count + " " + b);
                count = 1;
                b = a[i];
            }
        }
        System.out.println(count + " " + b);
    }
}
