package Task10;

import java.util.Random;
import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Random random = new Random(System.currentTimeMillis());
        int numberCPU = random.nextInt(1001);
        boolean isEnd = false;
        Scanner scanner = new Scanner(System.in);
        System.out.println(numberCPU);
        while (!isEnd) {
            int numberUser = scanner.nextInt();
            if (numberUser > 0) {
                isEnd = mainAlgoritm(numberCPU, numberUser);
            } else {
                isEnd = true;
            }
        }
    }

    public static boolean mainAlgoritm(int numberCPU, int numberUser) {
        if (numberUser > numberCPU) {
            System.out.println("Это число больше загаданного.");
            return false;
        } else if (numberUser < numberCPU) {
            System.out.println("Это число меньше загаданного.");
            return false;
        } else {
            System.out.println("Победа!");
            return true;
        }
    }
}
